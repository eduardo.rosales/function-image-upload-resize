namespace ImageFunctions
{
    public class ImageSizeReductionCalculator
    {
        private const double CONVERTION_FACTOR_BYTES_TO_MEGABYTES = 1024f;
        private const int THREE_MEGABYTES = 3;
        private long imageSizeBytes;
        private double imageSizeMegaBytes;

        public ImageSizeReductionCalculator(long imageSizeBytes)
        {
            this.imageSizeBytes = imageSizeBytes;

            SetImageSizeInMegaBytes();
        }

        public bool ImageIsBiggerThanThreeMegaBytes()
        {
            return imageSizeMegaBytes > THREE_MEGABYTES;
        }

        public double GetReductionPercentage()
        {
            if (imageSizeMegaBytes >= 3 && imageSizeMegaBytes <= 6)
            {
                return 0.9;
            }
            else if (imageSizeMegaBytes > 6 && imageSizeMegaBytes <= 12)
            {
                return 0.8;
            }
            else if (imageSizeMegaBytes > 12 && imageSizeMegaBytes <= 18)
            {
                return 0.5;
            }
            else if (imageSizeMegaBytes > 18)
            {
                return 0.3;
            }

            return 1;
        }

        private void SetImageSizeInMegaBytes()
        {
            imageSizeMegaBytes = (imageSizeBytes / CONVERTION_FACTOR_BYTES_TO_MEGABYTES) / CONVERTION_FACTOR_BYTES_TO_MEGABYTES;
        }
    }
}